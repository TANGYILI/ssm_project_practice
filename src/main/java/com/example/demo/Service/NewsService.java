package com.example.demo.Service;
import com.example.demo.Entity.News;
import com.example.demo.Mapper.NewsMapper;
import org.apache.catalina.mapper.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class NewsService {

    @Autowired
    NewsMapper newsMapper;

    public boolean AddNews(News news){

        newsMapper.insert(news);

        return true;
    }

    public boolean DeleteNews(int idnews){

        newsMapper.deleteByPrimaryKey(idnews);

        return true;
    }

    public boolean UpdateNews(News news) {

        newsMapper.updateByPrimaryKey(news);


        return true;
    }

    public List<News> QueryNews() throws SQLException {

        return newsMapper.selectAll();
    }

    public News GetNews(int idnews) throws SQLException {
        String sql="Select * from news where idnews='"+Integer.toString(idnews)+"'";
 return news;     }
        return newsMapper.selectByPrimaryKey(idnews);
    }
