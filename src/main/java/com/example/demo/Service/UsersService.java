package com.example.demo.Service;

import com.example.demo.Entity.Users;
import com.example.demo.Mapper.NewsMapper;
import com.example.demo.Mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UsersService {
    @Autowired
    UserMapper userMapper;

    public void AddUsers(){}
    public void DeleteUsers(){}
    public void UpdateUsers(){}
    public boolean QueryUsers(Users user) throws SQLException {

        return userMapper.select(user)!=null;
    }
    public void GetUsers(){}
}
