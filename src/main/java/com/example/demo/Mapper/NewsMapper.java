package com.example.demo.Mapper;

import com.example.demo.Entity.News;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

@Repository
public interface NewsMapper extends Mapper<News> {


}
