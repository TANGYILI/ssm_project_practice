package com.example.demo.Entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import javax.persistence.Id;
import java.sql.Date;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class News {
    @Id
    private int idnews=0;
    private String category="";
    private String title="";
    private String content="";
    private String author="";
    private Date newsdate;
}
