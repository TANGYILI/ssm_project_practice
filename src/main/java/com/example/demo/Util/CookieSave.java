package com.example.demo.Util;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

public class CookieSave {
    public void Save(HttpServletResponse response,String name,String value,int last_time)
    {
        //创建Cookie，将用户名存到叫cookieUserName的cookie中
        //Cookie 对象携带需要保存的数据，user.getName()=value，都是字符串类型
        //每个cookie保存一个数据，如果需要多个，创建多个cookie对象
        Cookie cookieUserName = new Cookie(name, value);
        cookieUserName.setMaxAge(last_time);
        response.addCookie(cookieUserName);
    }
}
