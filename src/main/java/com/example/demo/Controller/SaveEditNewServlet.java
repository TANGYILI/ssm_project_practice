package com.example.demo.Controller;

import com.example.demo.Service.NewsService;
import com.example.demo.Entity.News;
import org.springframework.stereotype.Controller;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;
/*
保存编辑的新闻
 */
@Controller
@WebServlet(name = "SaveNewServlet")
public class SaveEditNewServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        News news=new News();
        news.setAuthor(request.getParameter("author"));
        news.setTitle(request.getParameter("title"));
        news.setContent(request.getParameter("content"));
        news.setCategory(request.getParameter("category"));
        news.setNewsdate(Date.valueOf(request.getParameter("newsdate")));
        news.setIdnews(Integer.valueOf(request.getParameter("idnews")));
        NewsService newsService=new NewsService();
        System.out.println(newsService.UpdateNews(news));
        request.getRequestDispatcher("ShowNewsListServlet").forward(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
